# Welcome to Learning System Prove of Concept

## Requirements

...

## Keywords

- **Course** : Main course. There is only 1 Course right now.

- **Chapter** : This is the collection of Lesson.

- **Lesson** : There are 2 types for Lesson : *Lecture* and *Workshop*. (Lesson type)

    - **Lecture** : Text only. Include 2 type of contents *Article* and *Quiz*. Forward and Backward included in logic.

    - **Workshop** : Contain *Quiz* only. Forward only no Backward.

- **Account**

- **Status**
    
